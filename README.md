# Rows

A simple CSS gutter to create rows and cells using the flexbox model.

## Getting started 

Rows uses 12 units each row.

Simply add the class `bb-row` to a HTML container tag. The cells should get the class `bb-row__cell`. For example:

```
<div class="bb-row">
    <div class="bb-row__cell"></div>
    <div class="bb-row__cell"></div>
    <div class="bb-row__cell"></div>
</div>
```

Think about using another container to center these rows.

Use one of the modifier classes (like `bb-row__cell--cell2`) to define how many units a cell will take. For example: 


```
<div class="bb-row">
    <div class="bb-row__cell bb-row__cell--cell2">
        Uses 2 units
    </div>
    <div class="bb-row__cell bb-row__cell--cell6">
        Uses 6 units
    </div>
    <div class="bb-row__cell bb-row__cell--cell4">
        Uses 4 units
    </div>
</div>
```

Use classes like `bb-row__cell--rightMarginCell1` to define empty units to the left or to the right of a cell.

Use classes like `bb-row__cell--order2` to define the sort order of cells in case you want to use breakpoints and sort differently then.

## Customization

### Namespace 

You can easily change or remove the prefix if you want by changing the value of `$bb-rows-namespace`. 

### Gutter 

Use `$bb-rows-gutter` to define the size of your gutter.

## Help 

Feel free to contact us unser info@bitandblack.com